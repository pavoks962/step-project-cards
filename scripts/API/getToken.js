import instance from "./instance.js";

const getToken = async (body, form) => {
	try {
		const resp = await instance.post('/login', body);
		
		if (resp.status === 200) {
			const { data } = resp;
			return data;
		}

	} catch({ name, message, response }) {

		if(response) {
			
			if (response.status === 401) {
				console.error(`${name}: ${message}. ${response.data}`);
				form.showLoginError("Невірна адреса ел.пошти або пароль");
			}
			if (response.status === 500) {
				console.error(`${name}: ${message}.`);
				form.showLoginError("Будь ласка, введіть адресу ел.пошти та пароль.");
			}

		} else {
			form.showLoginError("Вибачте, щось пішло не так...");
		}
	}
}

export default getToken;