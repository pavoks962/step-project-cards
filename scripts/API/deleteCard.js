import instance from "./instance.js";

const deleteCard = async (cardId) => {
	try {
		return await instance.delete(`/${cardId}`);
	} catch(error) {
		console.log(error);
	}
}

export default deleteCard;