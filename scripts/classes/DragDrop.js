export default class DragDrop {
    constructor() {
        this.coordX = null;
        this.coordY = null;
    }

    getCapture() {
        this.cardList = document.querySelector(".cards__container");
        this.cardItem = document.querySelectorAll(".card__wrapper");
        this.cardItem.forEach((item) => {
   
            item.draggable = true;
            item.addEventListener(`dragstart`, (ev) => {
                this.coordX = ev.offsetX;
                this.coordY = ev.offsetY;
                console.log(this.coordY)
                ev.target.classList.add(`selected`);
                ev.dataTransfer.setData('text/plain', `dragstart`);
            });

            item.addEventListener(`dragend`, (ev) => {
                ev.dataTransfer.getData('text/plain', `dragstart`);
                ev.target.style.position = 'absolute';

                ev.target.style.top = `${ev.pageY - this.coordY}px`;
                ev.target.style.left = `${ev.pageX - this.coordX}px`;
                ev.target.zindex = 1000;
                ev.target.classList.remove(`selected`);
            });
        })
        this.cardList.addEventListener(`dragover`, (ev) => {
            ev.preventDefault();
        })
    }

    render() {
        this.getCapture();
    }
}


