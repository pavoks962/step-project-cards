import Form from "./Form.js"

export default class LoginForm extends Form {
	constructor() {
		super()
		this.error = document.createElement('div');
	}

	createElements() {
		super.createElements();
		this.form.className = "mb-3";

		this.form.insertAdjacentHTML('beforeend', `
			<label class="form-label">Email address</label>
				<input type="email" name="email" class="form-control mb-3" placeholder="name@example.com">
				<label class="form-label">Password</label>
			<input type="password" name="password" class="form-control"></input>
		`);

		this.error.style.color = 'red';
		this.error.className = 'mt-1';
		this.form.append(this.error);
	}

	showLoginError(message) {
		this.error.innerText = message;
	}
}

