import deleteCard from "../API/deleteCard.js";
import editCard from "../API/editCard.js";
import checkUserCards from "../functions/checkUserCards.js";
import VisitCardiologist from "./VisitCardiologist.js";
import VisitDentist from "./VisitDentist.js";
import VisitTherapist from "./VisitTherapist.js";
import DragDrop from "./DragDrop.js";


export default class Card {
    constructor (data) {
		this.data = data;
		this.id = data.id;
		this.editVisit = null;
		this.editVisitForm = null;

		this.cardWrapper = document.createElement('div');
        this.deleteBtnWrapper = document.createElement('div');
        this.deleteBtn = document.createElement('button');

        this.cardBody = document.createElement('div');
        this.cardMoreInfo = document.createElement('ul');

		this.infoEditBtnsWrapper = document.createElement('div');
        this.editBtn = document.createElement('button');
        this.detailsBtn = document.createElement('button');

		this.editContainer = document.createElement('div');
		this.editBtnsWrapper = document.createElement('div');
		this.confirmEditBtn = document.createElement('button');
		this.cancelEditBtn = document.createElement('button');
    }

    createElements () {
        this.cardWrapper.className = 'card__wrapper col card shadow border border-info';
   
		this.deleteBtnWrapper.className = 'text-end pt-2';
        this.deleteBtn.className = 'btn-close';
        this.deleteBtn.type = 'button';

        this.cardBody.className = 'card-body text-center pt-2';
        this.cardMoreInfo.className = 'card__more-info mt-1 list-group list-group-flush text-start';
		
		this.infoEditBtnsWrapper.className = 'more-info__btns-wrapper pb-4 text-center';
        this.editBtn.className = 'btn__edit-card btn btn-md text-light me-3';
		this.editBtn.innerText = 'Редагувати';
		this.detailsBtn.className = 'btn__details btn btn-md btn-info text-light';
		this.detailsBtn.innerText = 'Детальнішe';

		this.editBtnsWrapper.className = 'mt-4 mb-3 text-center';
		this.editBtnsWrapper.style.display = 'none';
		this.confirmEditBtn.className = 'btn btn-md btn__confirm-edit-card text-light me-3';
		this.confirmEditBtn.innerText = 'Підтвердити';
		this.cancelEditBtn.className = 'btn btn-md btn-info text-light';
		this.cancelEditBtn.innerText = 'Скасувати';

		this.displayVisitInfo();

		this.deleteBtnWrapper.append(this.deleteBtn);
		this.infoEditBtnsWrapper.append(this.editBtn, this.detailsBtn);

		this.editBtnsWrapper.append(this.confirmEditBtn, this.cancelEditBtn);
		this.editContainer.append(this.editBtnsWrapper);

		this.cardWrapper.append(this.deleteBtnWrapper, this.cardBody, this.infoEditBtnsWrapper, this.editContainer);
    
		const dragDrop = new DragDrop();
		dragDrop.render();

	}

	displayVisitInfo() {
		this.cardBody.innerHTML = `
			<p class="card__user-name card-header bg-info-subtle">${this.data.fullName}</p>
			<h5 class="card-title mt-4 pb-2">${this.data.doctor}</h5>
		`;

		this.cardMoreInfo.innerHTML = `
			<li class="list-group-item">
				<span class="fw-light fst-italic " style="color: red;">Статус: </span>
				${this.data.status}
			</li>
			<li class="list-group-item"> 
				<span class="fw-light fst-italic">Мета візиту: </span>
				${this.data.purpose}
			</li>
			<li class="list-group-item"> 
				<span class="fw-light fst-italic">Короткий опис візиту: </span>
				${this.data.descriptionVisit ? this.data.descriptionVisit : "-"}
			</li>
			<li class="list-group-item">
				<span class="fw-light fst-italic">Терміновість: </span>
				${this.data.priority}
			</li>
		`;

		switch (this.data.doctor) {
			case 'Кардіолог':
				this.cardMoreInfo.insertAdjacentHTML('beforeend', `
					<li class="list-group-item"> <span class="fw-light fst-italic">Тиск: </span>${this.data.pressure}</li>
					<li class="list-group-item"> <span class="fw-light fst-italic">Індекс маси тіла: </span>${this.data.weight}</li>
					<li class="list-group-item"> <span class="fw-light fst-italic">Перенесені захворювання серцево-судинної системи: </span>${this.data.diseases}</li>	
					<li class="list-group-item"> <span class="fw-light fst-italic">Вік: </span>${this.data.age}</li>
				`);
				break;

			case 'Стоматолог':
				this.cardMoreInfo.insertAdjacentHTML('beforeend', `
					<li class="list-group-item"> <span class="fw-light fst-italic">Дата останнього відвідування: </span>${this.data.date}</li>
				`);
				break;

			case 'Терапевт':
				this.cardMoreInfo.insertAdjacentHTML('beforeend', `
					<li class="list-group-item"> <span class="fw-light fst-italic">Вік: </span>${this.data.age}</li>
				`);
				break;
		}
		this.cardBody.append(this.cardMoreInfo);
	}


    addListeners () {
		
        this.deleteBtn.addEventListener('click', async () => {
			const resp = await deleteCard(this.id);
			if(resp?.status === 200) {
				this.delete();
				checkUserCards();
			}
		});


		this.detailsBtn.addEventListener('click', (ev) => {
            if (!ev.target.classList.contains('btn__hide-details')) {
                this.cardMoreInfo.style.display = 'block';
               	this.editBtn.style.display = 'inline-block';
               	this.detailsBtn.innerText = 'Приховати';
                this.detailsBtn.classList.add('btn__hide-details');
            } else {
				this.cardMoreInfo.style.display = '';
                this.editBtn.style.display = '';
                this.detailsBtn.innerText = 'Детальніше';
                this.detailsBtn.classList.remove('btn__hide-details');
            }
        })


        this.editBtn.addEventListener('click', () => {
			
			if (this.data.doctor === 'Кардіолог') {
				this.editVisit = new VisitCardiologist();
			}
			if (this.data.doctor === 'Стоматолог') {
				this.editVisit = new VisitDentist();
			}
			if (this.data.doctor === 'Терапевт') {
				this.editVisit = new VisitTherapist();
			}
	
			this.editVisitForm = this.editVisit.getFormElement();
			
			for (let key in this.data) {
				const field = this.editVisitForm.querySelector(`#${key}`);
				if (field) {
					field.value = this.data[key];
				}
			}
			this.showEditForm();
        })


		this.confirmEditBtn.addEventListener('click', async () => {
			const body = {
				...this.editVisit?.getValues(),
				doctor: this.data.doctor,
				id: this.id,
			};
			
			const response = await editCard(this.id, body);

			if (response) {
				this.data = response.data;
				this.hideEditForm();
				this.displayVisitInfo();
			}
		})

		this.cancelEditBtn.addEventListener('click', () => {
			this.hideEditForm();
		})
    }


	showEditForm() {
		this.cardBody.style.display = 'none';
		this.infoEditBtnsWrapper.style.display = 'none';
		this.editBtnsWrapper.style.display = 'block';
		this.editContainer.className = 'p-3';
		this.editContainer.prepend(this.editVisitForm);
	}

	hideEditForm() {
		this.cardBody.style.display = 'block';
		this.infoEditBtnsWrapper.style.display = 'block';
		this.editBtnsWrapper.style.display = 'none';
		this.editContainer.className = '';
		this.editVisitForm.remove();
	}

    delete () {
        this.cardWrapper.remove();
    }

    render (selector) {
        this.createElements();
        this.addListeners();
        document.querySelector(selector).prepend(this.cardWrapper);  
    }
}
