import { NO_ITEMS_WRAPPER } from "../constants/constants.js";
import getAllCards from "../API/getAllCards.js";

// -------- з перевіркою наявності карток на сервері -----------------------------

const checkUserCards = async () => {
	const noItemsMessage = document.querySelector(NO_ITEMS_WRAPPER);
	const cards = await getAllCards();

	noItemsMessage.style.display = cards.length === 0 ? 'block' : '';
}


// -------- з перевіркою наявності карток в дом дереві ---------------------------

// const checkUserCards = () => {
// 	const noItemsMessage = document.querySelector(NO_ITEMS_WRAPPER);
// 	const cards = document.querySelector(CARDS_CONTAINER).children;

// 	noItemsMessage.style.display = cards.length === 0 ? 'block' : '';
// }

export default checkUserCards;