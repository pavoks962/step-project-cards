import showUserAccount from "./showUserAccount.js";
import { LOCAL_STORAGE_TOKEN, LOGIN_BUTTON } from "../constants/constants.js";

const checkToken = () => {
	if (localStorage.getItem(LOCAL_STORAGE_TOKEN)) {
		showUserAccount();
	} else {
		document.querySelector(LOGIN_BUTTON).style.display = "block";
	}
}

export default checkToken;